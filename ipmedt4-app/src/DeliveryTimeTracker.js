import React from 'react';
import ReactDOM from "react-dom";
import "./DeliveryTimeTracker.css";
import axios from "axios";
import bezorger_image from './images/bezorger.jpg';

class DeliveryTimeTracker extends React.Component{
  state = {deliveryTime: "08:15", deliveryDate: "2020-01-07", MinutesTillDelivery: 0};


//  makeApiCall = searchTerm => {
//      const BASE_URL = "";
//      axios.get(BASE_URL + bestelling/orderNumber).then(res => {
//        this.setState({
//          deliveryTime: res.data.bezorgdatum,
//          deliveryDate: res.data.bezorgtijd
//        });
//      });
//    }
  makeApiCall(){
    axios.get("http://127.0.0.1:8000/api/bestelling/" + this.props.match.params.ordernummer).then(res => {
      this.setState({deliveryTime: res.data.bezorgtijd , deliveryDate: res.data.bezorgdatum})
    })
  }

  updateMinutesTillDelivery(){
    let splittedDeliveryTime = this.state.deliveryTime.split(':')
    let splittedDeliveryDate = this.state.deliveryDate.split('-')
    let hour = splittedDeliveryTime[0];
    let minute = splittedDeliveryTime[1];
    let day = splittedDeliveryDate[1];
    let month = splittedDeliveryDate[2] - 1;
    let year = splittedDeliveryDate[0];

    let currentTime = new Date();
    let deliveryTime = new Date(year, month, day, hour, minute);

    //Dit is in milliseconden /60000 om naar minuten te gaan
    let minutesTillDelivery = Math.round((deliveryTime - currentTime) / 60000);

    if(minutesTillDelivery < 0){
      this.setState({MinutesTillDelivery: 0});
    }
    else{
      this.setState({MinutesTillDelivery: minutesTillDelivery});
    }
  }

  componentDidMount(){
    this.makeApiCall()
    this.updateMinutesTillDelivery()
  };

    render(){
      var styling =  ((60-this.state.MinutesTillDelivery) / 60) * 1030;
      if(styling > 1030){
        styling = 1030
      }
      if(styling < 0){
        styling = 0
      }
      const style = {
        left: styling
      };

        return(
          <section className="BezorgtijdTracker">
            <section className="BezorgtijdTracker_text">
              <h2>Bezorgtijd over:</h2>
              <h1>{this.state.MinutesTillDelivery} minuten</h1>
            </section>
            <figure className="BezorgtijdTracker_bezorger">
              <img style={style} className="BezorgtijdTracker_bezorger_img" src={bezorger_image} alt="Delivery guy on a scooter"/>
            </figure>
          </section>
        );
    }
}

export default DeliveryTimeTracker;
