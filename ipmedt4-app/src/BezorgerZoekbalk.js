import React from "react";
import "./BezorgerZoekbalk.css";

class BezorgerZoekbalk extends React.Component {
  state={searchTerm: ""};

  onSearch = (event) => {
    this.setState({searchTerm: event.target.value});
  }

  onSubmit = (event) =>{
    event.preventDefault();
    this.props.onSubmit(this.state.searchTerm);
  }

  render(){
    return (
    <form  className="zoekbalk" onSubmit={this.onSubmit}>
        <input 
          onChange={this.onSearch} 
          className="zoekbalk_input" 
          type="text" 
          placeholder={this.props.placeholder} 
          value={this.state.searchTerm}
        />
    </form>
    );
  }
}

export default BezorgerZoekbalk;
