import React from 'react';
import classnames from 'classnames';
import './BetaalMethode.css';

import cash from "./images/cash.png";
import ideal from "./images/ideal.png";
import paypal from "./images/Paypal.png";
import mastercard from "./images/mastercard.png";
import visa from "./images/visa.png";



class BetaalMethode extends React.Component {

  render() {
    const cashclass = classnames('betaalmethode__section__list__item', {cashactive: this.state.cashactive});
    const idealclass = classnames('betaalmethode__section__list__item', {idealactive: this.state.idealactive});
    const paypalclass = classnames('betaalmethode__section__list__item', {paypalactive: this.state.paypalactive});
    const mastercardclass = classnames('betaalmethode__section__list__item', {mastercardactive: this.state.mastercardactive});
    const visaclass = classnames('betaalmethode__section__list__item', {idealactive: this.state.visaactive});

    return (
      <section className="betaalmethode__section">
        <ul className="betaalmethode__section__list">
          <li className={cashclass} onClick={this.cashClick.bind(this)}>
            <figure className="betaalmethode__section__list__item__figure">
              <img src={cash} className="betaalmethode__section__list__item__figure__image" alt=""/>
            </figure>
          </li>

          <li className={idealclass} onClick={this.idealClick.bind(this)}>
            <figure className="betaalmethode__section__list__item__figure">
              <img src={ideal} className="betaalmethode__section__list__item__figure__image" alt=""/>
            </figure>
          </li>

          <li className={paypalclass} onClick={this.paypalClick.bind(this)}>
            <figure className="betaalmethode__section__list__item__figure">
              <img src={paypal} className="betaalmethode__section__list__item__figure__image" alt=""/>
            </figure>
          </li>

          <li className={mastercardclass} onClick={this.mastercardClick.bind(this)}>
            <figure className="betaalmethode__section__list__item__figure">
              <img src={mastercard} className="betaalmethode__section__list__item__figure__image" alt=""/>
            </figure>
          </li>

          <li className={visaclass} onClick={this.visaClick.bind(this)}>
            <figure className="betaalmethode__section__list__item__figure">
              <img src={visa} className="betaalmethode__section__list__item__figure__image" alt=""/>
            </figure>
          </li>
        </ul>
      </section>
    );
  }

  /*
  Dit is miet de meest efficiente manier om dit te doen,
  *maar de andere methodes die ik heb geprobeerd was er de hele
  *tijd wel 1 ding dat niet werkte. Dus ik heb voor de manier
  *gekozen waarvan ik zeker wist dat het zou werken.
  */
  
  cashClick() {
    this.setState({ cashactive: true});
    this.setState({ idealactive: false});
    this.setState({ paypalactive: false});
    this.setState({ mastercardactive: false});
    this.setState({ visaactive: false});
  }

  idealClick() {
    this.setState({ cashactive: false});
    this.setState({ idealactive: true});
    this.setState({ paypalactive: false});
    this.setState({ mastercardactive: false});
    this.setState({ visaactive: false});
  }

  paypalClick() {
    this.setState({ cashactive: false});
    this.setState({ idealactive: false});
    this.setState({ paypalactive: true});
    this.setState({ mastercardactive: false});
    this.setState({ visaactive: false});
  }

  mastercardClick() {
    this.setState({ cashactive: false});
    this.setState({ idealactive: false});
    this.setState({ paypalactive: false});
    this.setState({ mastercardactive: true});
    this.setState({ visaactive: false});
  }

  visaClick() {
    this.setState({ cashactive: false});
    this.setState({ idealactive: false});
    this.setState({ paypalactive: false});
    this.setState({ mastercardactive: false});
    this.setState({ visaactive: true});
  }

  constructor(props) {
    super(props);
    this.state = {
      cashactive: false,
      idealactive: false,
      idealactive: false,
      idealactive: false,
      idealactive: false
    };
  }
}

export default BetaalMethode;
