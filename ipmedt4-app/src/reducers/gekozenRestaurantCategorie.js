const gekozenRestaurantCategorieReducer = (state = "", action) =>{
  switch(action.type){
    case 'CATEGORIE_GEKOZEN':
      return action.payload
    default:
      return state;
  }
};

export default gekozenRestaurantCategorieReducer;
