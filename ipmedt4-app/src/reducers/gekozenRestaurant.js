const gekozenRestaurantReducer = (state = "", action) =>{
  switch(action.type){
    case 'RESTAURANT_GEKOZEN':
      return action.payload
    default:
      return state;
  }
};

export default gekozenRestaurantReducer;
