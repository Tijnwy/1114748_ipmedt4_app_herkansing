const adresReducer = (state = "", action) =>{
  switch(action.type){
    case 'INGEVOERD_ADRES':
      return action.payload
    default:
      return state;
  }
};

export default adresReducer;
