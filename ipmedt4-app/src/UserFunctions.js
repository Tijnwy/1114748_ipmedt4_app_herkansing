import axios from 'axios';

export const register = newUser => {
    return axios
    .post(process.env.REACT_APP_API_URL + 'api/register', newUser, {
        headers:{'Content-Type': 'application/json'}
    })
    .then(res => {
        console.log(res);

    })
    .catch(err => {
        console.log(err);

    })
}

export const registerRestaurant = newRestaurant => {
    return axios
    .post(process.env.REACT_APP_API_URL + 'api/restaurant', newRestaurant, {
        headers:{'Content-Type': 'application/json'}
    })
    .then(res => {
        console.log(res);

    })
    .catch(err => {
        console.log(err);

    })
}


export const login = user => {
    return axios
        .post(
            process.env.REACT_APP_API_URL + 'api/login',
            {
                email: user.email,
                password: user.password
            },
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        .then(response => {
            localStorage.setItem('usertoken', response.data.token)
            console.log(response);
            if(response.statusText === "OK"){
                console.log("u bent succesvol ingelogd");
            }
            return response.data.token
        })
        .catch(err => {
            console.log(err)
            // alert("Onjuiste gegevens probeer het opnieuw!");
        })
}

export const getProfile = () => {
    // console.log("usertoken:" +localStorage.usertoken);
    var usertoken = localStorage.usertoken;

    try {
        return axios
        .get(process.env.REACT_APP_API_URL + 'api/profile', {

            headers: {
                Authorization: `Bearer ${usertoken}` }
        })
        .then(response => {
            return response.data
        })
        .catch(err => {
            console.log("get profile niet uitgevoerd")
        })
    } catch (error) {
        console.log("get profile niet uitgevoerd");
    }

}

export const registerFavoriet = newFavoriet => {
    return axios
    .post(process.env.REACT_APP_API_URL + '/favoriet', newFavoriet, {
        headers:{'Content-Type': 'application/json'}
    })
    .then(response => {
        console.log(response);

    })
    .catch(error => {
        console.log(error);

    })
}
