import React from 'react';
import axios from 'axios';
import {Link, withRouter} from 'react-router-dom';
import "./GerechtBewerken.css";
import  DeliveryPageHeader  from '.././DeliveryPageHeader';
import {getProfile} from '../UserFunctions';

class GerechtBewerken extends React.Component{

  constructor(props){
    super(props);
    this.state = {
      naam: '',
      prijs: '',
      beschrijving: '',
      foto: '',
      categorie: '',
      restaurant: '',
      categorieen: [],
      suggestions: []
    };
  }

  componentDidMount() {
    axios.get(process.env.REACT_APP_API_URL + 'api/product/' + this.props.match.params.id)
      .then(response => {
        this.setState(response.data);
      })
      .catch(error => console.log(error));

    // Om betaande categorieen op te halen
    getProfile().then(res => {
      axios
        .get('http://localhost:8000/restaurant/'+res.user.email)
        .then(response => {
          this.setState({restaurant: response.data.naam});

          // Haalt gerechten op gebaseerd op restaurant naam
          axios
            .get('http://localhost:8000/api/producten/'+response.data.naam)
            .then(response => {
              // Haalt de categorie van alle gerechten
              let gerechtCategorie = response.data.map((gerecht) => gerecht.categorie);

              // Verwijdert dubbele categorieen als deze eventueel bestaan
              let filteredCategorie = gerechtCategorie.filter(function(val, id){
                return gerechtCategorie.indexOf(val) == id;
              })
              this.setState({
                categorieen: filteredCategorie
              })
            })
        })
        .catch(error => {
          console.log(error);
        })
    })
  }

  goBack = (e) => {
    this.props.history.push('/gerechtoverzicht')
  };

  submitHandler = (e) => {
    e.preventDefault();
    axios
      .patch(process.env.REACT_APP_API_URL + 'api/product/update/'+this.state.id, this.state)
      .then(response =>{
        console.log(response);
        this.props.history.push('/gerechtoverzicht');
      })
      .catch(error => {
        console.log(error);
      })
  };

  openModal = ()=> {
    document.getElementById("gerVerw__background").style.display = "block";
  };

  closeModal = ()=> {
    document.getElementById("gerVerw__background").style.display = "none";
  };

  deleteHandler = (e) => {
    axios
      .delete(process.env.REACT_APP_API_URL + 'api/product/delete/'+this.state.id, this.state)
      .then(response => {
        console.log(response);
        this.props.history.push('/gerechtoverzicht');
      })
      .catch(error => {
        console.log(error);
      })
  };

  previewFile() {
    const preview = document.getElementById("bewerkForm__fotoPreview__foto");
    const file = document.getElementById('bewerkForm__fotoPreview__input').files[0];
    const reader = new FileReader();

    reader.addEventListener("load", function () {
      // Preview the selected image
      preview.src = reader.result;
    }, false);

    if (file != null) {
      reader.readAsDataURL(file);

      reader.onload = () => {
        var base64 = reader.result;
        var base64_string = base64.toString();
        this.setState({
          foto: base64_string
        })
      }
    }
  }

  onTextChanged = (e) => {
    const value = e.target.value;
    let suggestions = [];
    if(value.length > 0) {
      suggestions = this.state.categorieen.filter(
        (categorie) => {
          return categorie.toLowerCase().indexOf(value.toLowerCase()) !== -1;
        }
      );
    }
    else {
      suggestions = this.state.categorieen;
    }
    this.setState(() => ({
      suggestions: suggestions,
      categorie: value,
    }));
  }

  renderSuggestions() {
    const suggestions  = this.state.suggestions;
    if (suggestions.length === 0) {
      return null;
    }
    return (
      <ul>
        {suggestions.map((suggestion) =>
          <li key={suggestion} onClick={() => this.suggestionSelected(suggestion)}>{suggestion}</li>
        )}
      </ul>
    )
  }

  suggestionSelected (value) {
    this.setState(() => ({
      categorie: value,
      suggestions: [],
    }))
  }

  render(){
    return (
      <section id="gerechtForm__main">
      <DeliveryPageHeader
          titel="Welkom op het restaurantdashboard"
          ondertitel="De beheeromgeving voor jou als ondernemer."
        />
        <section id="bewerkForm__main">
          <section id="bewerkForm__titel">
            <section>
            <hr/>
            </section>
            <h2>Gerecht bewerken</h2>
            <section>
            <hr/>
            </section>
          </section>
          <section id="bewerkForm__section">
            <form id="bewerkForm" onSubmit={this.submitHandler} encType="multipart/form-data">
              <label>Gerecht titel:</label>
              <br/>
              <input
                id="bewerkForm__naam"
                className="bewerkForm__input"
                name="naam"
                value={this.state.naam}
                onChange={e => this.setState({[e.target.name]: e.target.value})}
                type="text"
                placeholder="Vul naam van gerecht in..."
                required
              />
              <br/>
              <label>Beschrijving:</label>
              <br/>
              <textarea
                id="bewerkForm__beschrijving"
                className="bewerkForm__input"
                name="beschrijving"
                value={this.state.beschrijving}
                onChange={e => this.setState({[e.target.name]: e.target.value})}
                type="text"
                placeholder="Beschrijf het gerecht..."
                rows="8"
                required
                >
              </textarea>
              <br/>
              <label>Foto van gerecht:</label>
              <br/>
              <section id="bewerkForm__fotoPreview">
                <input
                  id="bewerkForm__fotoPreview__input"
                  name="foto"
                  defaultValue={this.state.foto}
                  onChange={() => this.previewFile()}
                  type="file"
                />
                <img src="" id="bewerkForm__fotoPreview__foto"/>
              </section>
              <br/>
              <label id="bewerkForm__categorie--label">Categorie:</label>
              <br/>
              <input
                id="bewerkForm__categorie"
                autoComplete="off"
                name="categorie"
                value={this.state.categorie}
                onChange={this.onTextChanged}
                onFocus={(e) => {if(e.target.value < 1){this.setState({suggestions: this.state.categorieen})}else{this.onTextChanged(e)}}}
                onBlur={(e) => {if(e.target.value < 1){setTimeout(function(){this.setState({suggestions: []})}.bind(this), 120)}}}
                type="text"
                placeholder="Vul categorie in..."
                required
              />
              <section id="bewerkForm__categorieSuggestions">
                {this.renderSuggestions()}
              </section>
              <br/>
              <label>Prijs:</label>
              <br/>
              <section id="bewerkForm__prijs">
                <span>€</span>
                <input
                  id="bewerkForm__prijsInput"
                  name="prijs"
                  value={this.state.prijs}
                  onChange={e => this.setState({[e.target.name]: e.target.value})}
                  type="number"
                  step="0.01"
                  min="0"
                  placeholder="0.00"
                  required
                />
              </section>
              <br/>
              <section id="bewerkForm__buttons">
                <button onClick={this.openModal} id="bewerkForm__buttons__delete" type="button">Verwijder gerecht</button>
                <button id="bewerkForm__buttons__submit" type="submit">Gerecht opslaan</button>
              </section>
            </form>
            <br/>
            <button onClick={this.goBack} id="bewerkForm__buttons__reset">Annuleren</button>

            <section id="gerVerw__background">
              <section id="gerVerw">
                <section id="gerVerw__test">
                  <p id="gerVerw__text">Weet je zeker dat je dit gerecht wilt verwijderen?</p>
                </section>
                <section id="gerVerw__buttons">
                  <button onClick={this.closeModal} id="gerVerw__buttons__nee">Nee</button>
                  <button onClick={this.deleteHandler.bind(this, this.state)} id="gerVerw__buttons__ja">Ja</button>
                </section>
              </section>
            </section>

          </section>
        </section>
      </section>
    )
  }
}

export default GerechtBewerken;
