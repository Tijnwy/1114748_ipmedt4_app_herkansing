import React from 'react';
import {Link, withRouter} from 'react-router-dom';
import DeliveryPageHeader from '.././DeliveryPageHeader';

import "./RestaurantDashboard.css";

class RestaurantDashboard extends React.Component{
  render(){
    return (
      <section id="restDash__main">
        <DeliveryPageHeader
          titel="Welkom op het restaurantdashboard"
          ondertitel="De beheeromgeving voor jou als ondernemer."
        />
        <section id="restDash__head">
          <section>
            <hr/>
          </section>
          <h2>Ondernemer Dashboard</h2>
          <section>
            <hr/>
          </section>
        </section>
        <br/>
        <section id="restDash__body">
          <section id="restDash__gerechten" className="restDash__card">
            <p>Voeg hier nieuwe gerechten toe of bewerk gerechten</p>
            <Link to="gerechtoverzicht" className="restDash__button">Gerechten editor</Link>
          </section>
          <section id="restDash__bestellingen" className="restDash__card">
            <p>Zie en beheer hier geplaatste bestellingen bij uw restaurant</p>
            <Link to="restaurantbestellingen" className="restDash__button">Zie bestellingen</Link>
          </section>
          <section id="restDash__reviews" className="restDash__card">
            <p>Zie en reageer op geplaatste reviews over uw restaurant</p>
            <Link to="review" className="restDash__button">Zie reviews</Link>
          </section>
        </section>
      </section>
    )
  }
}

export default RestaurantDashboard;
