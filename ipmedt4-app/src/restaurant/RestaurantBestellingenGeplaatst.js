import React from 'react';
import axios from 'axios';
import {Link, withRouter} from 'react-router-dom';
import RestaurantBestellingenGeplaatstCard from './RestaurantBestellingenGeplaatstCard.js';
import {getProfile} from '../UserFunctions';

import "./RestaurantBestellingenGeplaatst.css";

class RestaurantBestellingenGeplaatst extends React.Component{
  constructor() {
    super();
    this.state = {
      bestellingen: []
    }
  }

  componentDidMount() {
    getProfile().then(res => {
      axios
        .get(process.env.REACT_APP_API_URL + 'restaurant/'+res.user.email)
        .then(response => {
          axios
          .get(process.env.REACT_APP_API_URL + 'bestelling/restaurant/'+ response.data.naam)
          .then(response => {
            this.setState({
              bestellingen: response.data
            })
          })
          .catch(error => {
            console.log(error);
          })
        })
        .catch(error => {
          console.log(error);
        })
    })
  }

  collapseList = () => {
    document.getElementById("geplBest__ul").style.height = "0";
    document.getElementById("geplBest__titel__collapse").style.display = "none";
    document.getElementById("geplBest__titel__expand").style.display = "inline-block";
  }

  expandList = () => {
    document.getElementById("geplBest__ul").style.height = "45rem";
    document.getElementById("geplBest__titel__collapse").style.display = "inline-block";
    document.getElementById("geplBest__titel__expand").style.display = "none";
  }

  updateBestellingen = () => {
    getProfile().then(res => {
      axios
        .get(process.env.REACT_APP_API_URL + 'restaurant/'+res.user.email)
        .then(response => {
          axios
          .get(process.env.REACT_APP_API_URL + 'bestelling/restaurant/'+ response.data.naam)
          .then(response => {
            this.setState({
              bestellingen: response.data
            })
          })
          .catch(error => {
            console.log(error);
          })
        })
        .catch(error => {
          console.log(error);
        })
    })
  }

  refreshPage = () => {
    this.props.refreshPage();
  }

  render(){
    return (
      <section id="geplBest__section">
        <section id="geplBest">
          <section id="geplBest__titel">
            <h3>Geplaatste bestellingen</h3>
            <section id="geplBest__titel__buttons">
              <button onClick={this.collapseList} id="geplBest__titel__collapse"><i className="arrow up" /></button>
              <button onClick={this.expandList} id="geplBest__titel__expand"><i className="arrow down" /></button>
            </section>
          </section>
          <ul id="geplBest__ul">
            {this.state.bestellingen.reverse().filter(bestelling => bestelling.status === "geplaatst").map(bestelling =>
              <RestaurantBestellingenGeplaatstCard key={bestelling.ordernummer} orderNummer={bestelling.ordernummer} updateBestellingen={this.refreshPage} />
            )}
          </ul>
        </section>
      </section>
    )
  }
}

export default RestaurantBestellingenGeplaatst;
