import React from 'react';
import axios from 'axios';
import {Link, withRouter} from 'react-router-dom';
import RestaurantBestellingenGeplaatst from './RestaurantBestellingenGeplaatst.js';
import RestaurantBestellingenGemaakt from './RestaurantBestellingenGemaakt.js';
import RestaurantBestellingenBezorgd from './RestaurantBestellingenBezorgd.js';

import updateLogo from "../images/update-arrow2.png"

import "./RestaurantBestellingen.css";
import DeliveryPageHeader from '.././DeliveryPageHeader';

class RestaurantBestellingen extends React.Component{
  constructor(){
    super();
    this.child1Ref = React.createRef();
    this.child2Ref = React.createRef();
    this.child3Ref = React.createRef();
  }

  refreshPage = () => {
    const refreshButton = document.getElementById("restBest__refresh__button");
    const refreshLogo = document.getElementById("restBest__refresh__logo");

    refreshButton.disabled = true;
    refreshButton.style.opacity = "0.5";
    refreshLogo.style.display = "block";

    // Verwijdert de hover effect wanneer button inactief is
    const disableHover = '#restBest__refresh__button:hover{border: solid .2rem #008FE2;}';
    const style = document.createElement('style');

    if(style.styleSheet) {
      style.styleSheet.cssText = disableHover;
    } else {
      style.appendChild(document.createTextNode(disableHover));
    }

    refreshButton.appendChild(style);

    setTimeout(function(){refreshButton.disabled = false;},1000);
    setTimeout(function(){refreshButton.style.opacity = "1";},1000);
    setTimeout(function(){refreshLogo.style.display = "none";},1000);
    setTimeout(function(){refreshButton.removeChild(style);},1000);

    setTimeout(function() {
      this.child1Ref.current.updateBestellingen();
      this.child2Ref.current.updateBestellingen();
      this.child3Ref.current.updateBestellingen();
    }.bind(this), 200);
  }

  render(){
    return (
      <section id="restBest__main">
        <DeliveryPageHeader
          titel="Welkom op het restaurantdashboard"
          ondertitel="De beheeromgeving voor jou als ondernemer."
        />
        <section id="restBest__titel">
          <section>
            <hr/>
          </section>
          <h2>Bestellingen</h2>
          <section>
            <hr/>
          </section>
        </section>
        <br/>
        <section id="restBest__refresh">
          <button id="restBest__refresh__button" onClick={this.refreshPage} disabled={false}>Refresh</button>
          <img id="restBest__refresh__logo" src={updateLogo} alt="Reloading..."/>
        </section>
        <br/>
        <section id="restBest__lijsten">
          <RestaurantBestellingenGeplaatst ref={this.child1Ref} refreshPage={this.refreshPage}/>
          <RestaurantBestellingenGemaakt ref={this.child2Ref}/>
          <RestaurantBestellingenBezorgd ref={this.child3Ref}/>
        </section>
      </section>
    )
  }
}

export default RestaurantBestellingen;
