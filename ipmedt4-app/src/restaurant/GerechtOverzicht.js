import React from 'react';
import axios from 'axios';
import {Link, withRouter} from 'react-router-dom';
import {getProfile} from '../UserFunctions';

import "./GerechtOverzicht.css";
import  DeliveryPageHeader  from '.././DeliveryPageHeader';

class GerechtOverzicht extends React.Component{
  constructor() {
    super();
    this.state = {
      gerechten: [],
      search:''
    }
  }

  //Haalt gerechten op van het restaurant dat is ingelogd
  componentDidMount() {
    getProfile().then(res => {
      axios
        .get(process.env.REACT_APP_API_URL + 'restaurant/'+res.user.email)
        .then(response => {
          axios
            .get(process.env.REACT_APP_API_URL + 'api/producten/'+response.data.naam)
            .then(response => {
              this.setState({
                gerechten: response.data
              })
            })
            .catch(error => {
              console.log(error);
            })
        })
        .catch(error => {
          console.log(error);
        })
    })
  }

  //Update de state naar wat er in de zoekbalk wordt getypt
  updateSearch(event) {
    this.setState({search: event.target.value.substr(0,20)});
  }

  render(){
    let filteredGerechten = this.state.gerechten.filter(
      (gerecht) => {
        return gerecht.naam.toLowerCase().indexOf(this.state.search.toLowerCase()) !== -1;
      }
    );
    return (
      <section id="gerOver__main">
      <DeliveryPageHeader
          titel="Welkom op het restaurantdashboard"
          ondertitel="De beheeromgeving voor jou als ondernemer."
        />
        <section id="gerOver__head">
          <section>
            <hr/>
          </section>
          <h2>Gerechten</h2>
          <section>
            <hr/>
          </section>
        </section>
        <br/>
        <section id="gerOver__body">
          <Link to="gerechtinvoer" id="gerOver__toevoegenButton">Voeg gerecht toe</Link>
          <br/>
          <input
            onChange={this.updateSearch.bind(this)}
            id="gerOver__searchBar"
            type="text"
            placeholder="Zoek op gerecht naam..."
            value={this.state.search}
          />
          <ul id="gerOver__grid">
            {filteredGerechten.map(product =>
              <li className="gerOver__card" key={product.id}>
                <p className="gerOver__card__naam">{product.naam}</p>
                <img className="gerOver__card__foto" src={product.foto} />
                <p className="gerOver__card__categorie">{product.categorie}</p>
                <p className="gerOver__card__prijs">€{product.prijs}</p>
                <section className="gerOver__card__beschrijving">
                  <p className="gerOver__card__beschrijving__tekst">{product.beschrijving}</p>
                </section>
                <section className="gerOver__card__spaces"></section>
                <Link to={"gerechtbewerken/" + product.id} className="gerOver__card__button">Bewerk gerecht</Link>
              </li>
            )}
          </ul>
        </section>
      </section>
    )
  }
}

export default GerechtOverzicht;
