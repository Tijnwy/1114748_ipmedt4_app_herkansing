import React from 'react';
import axios from 'axios';
import {Link, withRouter} from 'react-router-dom';
import RestaurantBestellingenGeplaatstBon from './RestaurantBestellingenGeplaatstBon.js';
import RestaurantKlantNaam from './RestaurantKlantNaam.js';

import "./RestaurantBestellingenGeplaatstModal.css";

class RestaurantBestellingenGeplaatstModal extends React.Component{
  constructor() {
    super();
    this.state = {
      ordernummer: '',
      totaalbedrag: '',
      bezorgdatum: '',
      bezorgtijd: '',
      betaalmethode: '',
      email_klant: '',
      email_bezorger: '',
      naam_restaurant: '',
      status: '',
      bezorgadres: ''
    }
  }

  componentDidMount() {
    axios
    .get(process.env.REACT_APP_API_URL + 'bestelling/'+this.props.orderNummer)
    .then(response => {
      this.setState(response.data);
    })
    .catch(error => {
      console.log(error);
    })
  }

  closeModal = () => {
    const modalId = "geplBest__modal"+this.state.ordernummer;
    document.getElementById(modalId).style.display = "none";
  };

  setStatus = (e) => {
    this.setState({
      status: 'gemaakt'
    });
    //Een timeout om de status te laten veranderen
    setTimeout(
      function() {this.submitOrder(e)}
      .bind(this),
      10
    );
  }

  submitOrder = (e) => {
    axios
      .patch(process.env.REACT_APP_API_URL + 'api/bestelling/update/'+this.state.ordernummer, this.state)
      .then(response =>{
        this.closeModal();
        setTimeout(
          function() {this.props.updateBestellingen()}
          .bind(this),
          10
        );
      })
      .catch(error => {
        console.log(error);
      })
  };

  render(){
    //Voorkomt dat de child component een lege prop krijgt opgestuurd
    let restaurantBestellingenGeplaatstBon;
    if(this.state.ordernummer > 0) {
      restaurantBestellingenGeplaatstBon = <RestaurantBestellingenGeplaatstBon orderNummer={this.state.ordernummer}/>;
    }
    else {restaurantBestellingenGeplaatstBon = null}

    let restaurantKlantNaam;
    if(this.state.ordernummer > 0) {
      restaurantKlantNaam = <RestaurantKlantNaam emailKlant={this.state.email_klant}/>;
    }
    else {restaurantKlantNaam = null}

    return (
      <section id="bestModal">
        <section id="bestModal__titel">
          <p>Ordernummer #{this.state.ordernummer}</p>
        </section>
        <section id="bestModal__details">
          <section id="bestModal__details__klant">
            <p className="bestModal__details__klant__p">{restaurantKlantNaam}</p>
            <p className="bestModal__details__klant__p">{this.state.bezorgadres}</p>
            <p className="bestModal__details__klant__p">Bezorgtijd: {this.state.bezorgtijd}</p>
          </section>
          <section id="bestModal__details__bestelling">
            {restaurantBestellingenGeplaatstBon}
          </section>
        </section>
        <section id="bestModal__buttons">
          <button onClick={this.closeModal} id="bestModal__buttons__return">Terug naar overzicht</button>
          <button id="bestModal__buttons__confirm" onClick={this.setStatus}>Klaar voor bezorging</button>
        </section>
      </section>
    )
  }
}

export default RestaurantBestellingenGeplaatstModal;
