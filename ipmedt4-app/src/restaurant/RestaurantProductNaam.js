import React from 'react';
import axios from 'axios';
import {Link, withRouter} from 'react-router-dom';

import "./RestaurantProductNaam.css";

class RestaurantKlantNaam extends React.Component{
  constructor() {
    super();
    this.state = {
      product: []
    }
  }

  componentDidMount() {
    axios
    .get(process.env.REACT_APP_API_URL + 'api/product/'+this.props.productId)
    .then(response => {
      this.setState({
        product: response.data
      })
    })
    .catch(error => {
      console.log(error);
    })
  }

  render(){
    return (
      <p className="card__naamProduct">{this.state.product.naam}</p>
    )
  }
}

export default RestaurantKlantNaam;
