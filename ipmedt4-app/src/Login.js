import React from 'react';
import {login} from './UserFunctions.js';
import "./Login.css";
import image from './images/login2.png';
import LandingspageHeader from './LandingspageHeader';
import {Link, withRouter} from 'react-router-dom';
import axios from 'axios';
import {getProfile} from "./UserFunctions"


class Login extends React.Component{
    constructor(){
        super();
        this.state = {
            email: '',
            password: '',
            kleur:'white',
            validatietext:'',
            rol: '',
            errors: {}
        }

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onChange(e){
    this.setState({[e.target.name]: e.target.value })
}




onSubmit(e){
    e.preventDefault();
    const user ={
        email:this.state.email,
        password: this.state.password
    }
    console.log(user);
    login(user).then(res => {
        if (res) {
           getProfile().then(res => {
             console.log(res);
            this.setState({
                rol: res.user.rol
            })

            if(this.state.rol === 'klant'){
                this.props.history.push(`/`);
                setTimeout(function(){ alert("Succesvol ingelogd!"); }, 500);
               }

            else if(this.state.rol === 'restaurant'){
                this.props.history.push(`/restaurantdashboard`);
                setTimeout(function(){ alert("Succesvol ingelogd!"); }, 500);
             }

             else if(this.state.rol === 'bezorger'){
                this.props.history.push(`/bezorgerpagina`);
                setTimeout(function(){ alert("Succesvol ingelogd!"); }, 500);
               }
        })
    }

        else{
            console.log("inloggen niet gelukt");
            this.setState({
                kleur: 'red',
                validatietext: 'Onjuiste gegevens'
            })
        }
    })
}



render(){
    return(
    <section>
        <LandingspageHeader/>
        <section className="login">
            <section className="login_back">
                <Link to="" className="login_back_arrow">&#8592;</Link>
            </section>
            <form className="login_form" onSubmit={this.onSubmit}>
                <section className="login_form_header">
                    <hr className="login_form_header_line"/>
                    <h1 className="login_form_header_h1">Log in</h1>
                    <hr className="login_form_header_line" />
                </section>

                <section className="login_form_main">
                    <section className="login_form_main_system">
                    <label className="login_form_main_system_label" htmlFor="email">Emailadres</label>
                    <br/>
                    <input type="email"
                    className="login_form_main_system_input"
                    name="email"
                    placeholder="Typ hier de mail"
                    value={this.state.email}
                    onChange={this.onChange}
                    required
                    />

                    <br/>
                    <label className="login_form_main_system_label" htmlFor="password">Password</label>
                    <br/>
                    <input type="password"
                    className="login_form_main_system_input"
                    name="password"
                    placeholder="Typ hier het wachtwoord"
                    value={this.state.password}
                    onChange={this.onChange}
                    required
                    />
                    <small className="login_form_main_system_small" style={{ color: this.state.kleur}}>{this.state.validatietext}</small>



                    <br/><button className="login_form_main_system_submit" type="submit"> Log in</button>
                    <a className="login_form_main_system_password" href="/">Wachtwoord vergeten?</a>
                    </section>
                    <section className="login_form_main_side">
                        <figure className="login_form_main_side_figure">
                            <img className="login_form_main_side_figure_img" src={image} alt="Avatar afbeelding"/>
                        </figure>
                    </section>
                </section>
            </form>
        </section>
    </section>
    );
}

}

export default Login;
