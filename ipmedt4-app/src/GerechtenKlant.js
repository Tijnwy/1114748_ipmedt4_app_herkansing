import React from 'react';
// import {BrowserRouter as Router, Route} from "react-router-dom";
import DeliveryPageHeader from './DeliveryPageHeader.js';
import axios from 'axios';
import {keuzeRestaurantActie, keuzeRestaurantCategorieActie} from './actions';
import { connect } from 'react-redux';
import "./GerechtenKlant.css";
import GerechtenKlantBanner from './GerechtenKlantBanner.js';
import GerechtenKlantMenu from './GerechtenKlantMenu.js';
import GerechtenKlantKeuze from './GerechtenKlantKeuze.js';
import ReviewModal from './ReviewModal.js';
import WriteReviewModal from "./WriteReviewModal.js";
import GerechtenWinkelmandje from './GerechtenWinkelmandje.js';
import {getProfile} from './UserFunctions.js';

class GerechtenKlant extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      restaurantCardNaam: "",
      restaurantCardLogo: "",
      restaurantCardRating: "",
      restaurantCardInfoBeschrijving: "",
      restaurantCategorie: "",
      producten: [],
      categorieProductenState: [],
      productenInWinkelmandjeState: [],
      totaalBedragState: 0,
      categorieLijst: [],
      showReviewModal: false,
      showWriteReviewModal: false,
      categorieProductenStateFilter: []
    }
    this.productenInWinkelmandje = [];
    this.totaalBedrag = 0;
    this.bestelling = {};
    this.componentUpdate = false;
    this.firstUpdate = true;
    this.closeReviewModal = this.closeReviewModal.bind(this);
    this.showReviewModal = this.showReviewModal.bind(this);
    this.closeWriteReviewModal = this.closeWriteReviewModal.bind(this);
    this.showWriteReviewModal = this.showWriteReviewModal.bind(this);
  }

//Voor nu is /mac hardcoded restaurant. Wanneer veel dummy data in seeders gebruik:
// +this.props.gekozenRestaurant (redux afkomstig uit klantbestellen)
componentDidMount(){

  axios.get(process.env.REACT_APP_API_URL + 'api/producten/' + this.props.gekozenRestaurant).then(res =>{
      this.setState({producten: res.data})
  })

  axios.get(process.env.REACT_APP_API_URL + 'api/restaurant/' + this.props.gekozenRestaurant +  '/info').then(res =>{
      console.log(res);
      this.setState({restaurantCardNaam: res.data[0].naam})
      this.setState({restaurantCardLogo: res.data[0].logo})
      this.setState({restaurantCardRating: res.data[0].rating})
      this.setState({restaurantCardBeschrijving: res.data[0].beschrijving})
      this.setState({restaurantCategorie: res.data[0].categorie})
    });
  }

  componentDidUpdate(){
    if(this.firstUpdate){
      this.makeCategorieProducten()
    }
  }

  showReviewModal(naam){
    this.setState({showReviewModal: true});
  }

  closeReviewModal(){
    this.setState({showReviewModal: false});
  }

  showWriteReviewModal(){
    this.setState({showWriteReviewModal: true});
    this.setState({showReviewModal: false});
  }

  closeWriteReviewModal(){
    this.setState({showWriteReviewModal: false});
  }

  reviewModal(){
    let reviewModal;
    if(this.state.showReviewModal === true){
      return <ReviewModal showWriteReviewModal={this.showWriteReviewModal} restaurantNaam={this.state.restaurantCardNaam} closeReviewModal={this.closeReviewModal}/>
    }
    if(this.state.showWriteReviewModal === true){
      return <WriteReviewModal restaurantNaam={this.state.restaurantCardNaam} closeReviewModal={this.closeReviewModal} closeWriteReviewModal={this.closeWriteReviewModal}/>
    }
    else{
      console.log("geen review modal");
    }
  }

//Maak van de state producten een array van de categorieën
//daarin zit een array van de producten, per index is het gevuld met het object product.
  makeCategorieProducten = () =>{
  let categorieLijstUniek = []
  let categorieProducten = []
  //loop eerst voor categorieën
  for(let i = 0; i < this.state.producten.length; i++){
    console.log(this.state.producten[i].categorie + "empty?")
    categorieLijstUniek.push(this.state.producten[i].categorie)
  }
}
//Maak een array die gevuld wordt met objecten die de categorie definiëren
//Voeg bij de array een nieuw object die een array van producten heeft. Dit nieuwe object
//productenlijst wordt bij de juiste categorie object samen gevoegd.
  makeCategorieProducten = () =>{
    let categorieLijstUniek = []
    let categorieProducten = []
    for(let i = 0; i < this.state.producten.length; i++){
      console.log(this.state.producten[i].categorie + "empty?")
      categorieLijstUniek.push(this.state.producten[i].categorie)
    }
    //maak de lijst uniek
    categorieLijstUniek = [...new Set(categorieLijstUniek)]

    //Maak het object producten lijst aan en dat is een array.
    for(let i = 0; i < categorieLijstUniek.length; i++){
        categorieProducten[i] = {categorie: categorieLijstUniek[i]}
        categorieProducten[i].productLijst = []
    }

    //Vul het object array productlijst met verschillende producten die bij de juiste categorie horen
    for(let i = 0; i < categorieLijstUniek.length; i++){
      for(let x = 0; x < this.state.producten.length; x++){
        if(categorieLijstUniek[i] == this.state.producten[x].categorie){
          categorieProducten[i].productLijst.push({product: this.state.producten[x]})
        }
      }
    }
    console.log(categorieProducten)
    this.firstUpdate = false;
    this.setState({categorieProductenState: categorieProducten})
    this.setState({categorieLijst: categorieProducten})
  }

  navigeerBetaal = () =>{
      //window.location.href="/betalen"
      this.maakBestelling()
  }

  getEmail = () => {
    if(localStorage.getItem('usertoken') != null) {
      getProfile().then(res => {
        console.log(res);
        
        let email = res.user.email;
        return email;    
      }) 
    } else {
      return null;
    }
  }

  getDatum = () => {
    let datum = new Date().toISOString();
    datum = datum.slice(0,10);
    return datum;
  }

  getTijd = () => {
    let tijd = new Date(new Date().getTime() + 150*60000);
    tijd = tijd.toISOString().slice(11,16)
    return tijd;
  }

  getBezorgAdres = () => {
    if(localStorage.getItem('usertoken') != null) {
      getProfile().then(res => {
        console.log(res);
        
        let bezorgadres = res.user.straatnaam + " " + res.user.huisnummer + " " + res.user.woonplaats;
        return bezorgadres;    
      }) 
    } else {
      let bezorgadres = "Pieter Wouterstraat 13 Voorhout";
      return bezorgadres;
    }
  }

  maakBestelling = () => {
    //Post de volgende gegevens:
    this.bestelling.totaalbedrag = this.totaalBedrag;
    this.bestelling.naam_restaurant = "mac";
    this.bestelling.email_klant = this.getEmail();
    this.bestelling.bezorgtijd = this.getTijd();
    this.bestelling.bezorgdatum = this.getDatum()
    this.bestelling.bezorgadres = this.getBezorgAdres();
    console.log(this.bestelling) //verander later naar redux vvar
    axios.post(process.env.REACT_APP_API_URL + 'api/bestelling/create', this.bestelling)
      .then(response =>{
        axios.get(process.env.REACT_APP_API_URL + 'api/bestelling/laatsteBestelling')
        .then(response =>{
          console.log(response.data.ordernummer)
          this.plaatsBestelling(response.data.ordernummer)
        })
        .catch(error =>{
          console.log(error)
        })
      })
      .catch(error => {
        console.log(error.response.headers);
        console.log(error.response.data);
      })
  };

  plaatsBestelling = (ordernummer) => {
    let productInBestelling = JSON.parse(JSON.stringify(this.state.productenInWinkelmandjeState));
    for(let i = 0; i < this.state.productenInWinkelmandjeState.length; i++){
      console.log(productInBestelling)
      productInBestelling[i].ordernummer = ordernummer
      axios.post(process.env.REACT_APP_API_URL + 'api/productinbestelling/create', productInBestelling[i])
        .then(response =>{
          //Naar volgende pagina
          this.props.history.push('/bezorgpagina/' + ordernummer);
          console.log(response)
        })
        .catch(error =>{
          console.log(error)
        })
      }
  }

  //Gerecht en prijs
  gerechtClicked = (gerecht, prijs, id) =>{
    let objectGerechtPrijs = {}
    let pushNaarList = true;
    this.totaalBedrag = 0;
    objectGerechtPrijs.gerecht = gerecht;
    objectGerechtPrijs.prijs = prijs;
    objectGerechtPrijs.aantal = 1;
    objectGerechtPrijs.id = id;
    for(let i = 0; i < this.productenInWinkelmandje.length; i++){
      if(this.productenInWinkelmandje[i].gerecht.includes(gerecht)){
        this.productenInWinkelmandje[i].aantal += 1;
        this.productenInWinkelmandje[i].prijs = (objectGerechtPrijs.prijs * this.productenInWinkelmandje[i].aantal).toFixed(2);
        pushNaarList = false;
      }
    }
    if(pushNaarList){
      this.productenInWinkelmandje.push(objectGerechtPrijs);
    }

    for(let i = 0; i < this.productenInWinkelmandje.length; i++){
      this.totaalBedrag = (parseFloat(this.totaalBedrag) + parseFloat(this.productenInWinkelmandje[i].prijs)).toFixed(2);
    }
    console.log(this.totaalBedrag +  "het echte bedrag")
    this.setState({productenInWinkelmandjeState: this.productenInWinkelmandje})
    this.setState({totaalBedragState: this.totaalBedrag})
    /* if(this.productenInWinkelmandje.gerecht.includes(objectGerechtPrijs.gerecht)){
      console.log("hij is dubbel")
    } */

  }


  deleteItem = (nummer) =>{
    let indexWinkelmandje = this.state.productenInWinkelmandjeState.findIndex(x => x.id === nummer)
    let indexProducten = this.state.producten.findIndex(x => x.id === nummer)

    this.totaalBedrag -= this.state.producten[indexProducten].prijs;
    this.totaalBedrag = parseFloat(this.totaalBedrag).toFixed(2)
    this.setState({totaalBedragState: this.totaalBedrag})

    if(this.productenInWinkelmandje[indexWinkelmandje].aantal > 1){
      this.productenInWinkelmandje[indexWinkelmandje].aantal -= 1;
      this.productenInWinkelmandje[indexWinkelmandje].prijs -= this.state.producten[indexProducten].prijs;
      this.productenInWinkelmandje[indexWinkelmandje].prijs = parseFloat(this.productenInWinkelmandje[indexWinkelmandje].prijs).toFixed(2)
      this.setState({productenInWinkelmandjeState: this.productenInWinkelmandje})
    }
    else{
      this.productenInWinkelmandje.splice(indexWinkelmandje, 1)
      this.setState({productenInWinkelmandjeState: this.productenInWinkelmandje})
    }
  }

  categorieGekozen = (categorie) =>{
    console.log(categorie)
    if(categorie === 'Zie Alle'){
      this.setState({categorieProductenStateFilter: []});
      this.makeCategorieProducten()
    }
    else{
      let index = this.state.categorieProductenState.findIndex(x => x.categorie == categorie);
      console.log(index)
      let nieuwecategorie = [this.state.categorieProductenState[index]]
      console.log(nieuwecategorie)
       this.setState({categorieProductenStateFilter: nieuwecategorie});
    }
  }

  render(){
    let toegevoegdeGerechten = this.state.productenInWinkelmandjeState || [];
    let categorieChildState = []
    console.log(this.state.categorieProductenStateFilter)
    if(this.state.categorieProductenStateFilter.length !== 0){
      categorieChildState = this.state.categorieProductenStateFilter
    }
    else{
      categorieChildState = this.state.categorieProductenState
    }
    console.log(this.state.categorieProductenState)
    return(
      <section>
        <GerechtenWinkelmandje totaalBedrag={this.state.totaalBedragState} deleteItem={this.deleteItem} navigeerBetaal={this.navigeerBetaal} toegevoegdeGerechten={toegevoegdeGerechten} />
        <DeliveryPageHeader ondertitel="Steun deze lokale ondernemer!" titel={"Bestel bij " + this.state.restaurantCardNaam + " !"|| "Jouw favoriete Restaurant"}/>
        <GerechtenKlantBanner showReviewModal={this.showReviewModal} naam={this.state.restaurantCardNaam} logo={this.state.restaurantCardLogo}
        rating={this.state.restaurantCardRating} beschrijving={this.state.restaurantCardBeschrijving} categorie={this.state.restaurantCategorie}/>
        {this.reviewModal()}
        <GerechtenKlantMenu categorieGekozen={this.categorieGekozen} categorie={this.state.categorieLijst}/>
        {
          categorieChildState.map((categorie)=>
          <div>
          <h2 className="CategorieTitel">{categorie.categorie}</h2>
          <main className="GerechtenKlant_main">
          {categorie.productLijst.map((sub)=>
              <div>
              <GerechtenKlantKeuze id={sub.product.id} gerechtClicked={this.gerechtClicked} titel={sub.product.naam} imgSrc={this.state.restaurantCategorie} beschrijving={sub.product.beschrijving} prijs={sub.product.prijs}/>
              </div>
          )}</main>
          </div>
        )
        }
      </section>

    );
  }
}



const mapStateToProps = (state) => ({
  gekozenRestaurant: state.gekozenRestaurant,
  gekozenCategorie: state.gekozenCategorie
});

const mapDispatchToProps = {
  keuzeRestaurantActie,
  keuzeRestaurantCategorieActie
}

export default connect(mapStateToProps, mapDispatchToProps ) (GerechtenKlant);
