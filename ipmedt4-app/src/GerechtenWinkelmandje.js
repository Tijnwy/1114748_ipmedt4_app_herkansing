import React from 'react';
import './GerechtenWinkelmandje.css'
import star from "./images/star.png";
import WinkelmandjeItem from "./WinkelmandjeItem"

class GerechtenWinkelmandje extends React.Component{

  /*  {this.props.toegevoegdeGerechten.map((gerechten, index)=>
      <WinkelmandjeItem  key={index} aantal={gerechten.aantal} gerecht={gerechten.gerecht} prijs={gerechten.prijs}/>
    )} */

  deleteItem = (nummer) =>{
    this.props.deleteItem(nummer);
  }

  render(){
    return(
      <section className="GerechtenWinkelmandje">
        <ul className="GerechtenWinkelmandje_lijst">
         {this.props.toegevoegdeGerechten.map((gerechten)=>
            <WinkelmandjeItem deleteItem={this.deleteItem} nummer={gerechten.id} key={gerechten.id} aantal={gerechten.aantal} gerecht={gerechten.gerecht} prijs={gerechten.prijs}/>
          )}
        </ul>
        <h1 class="GerechtenWinkelmandje_totaalBedrag">Totaal bedrag = {this.props.totaalBedrag}€</h1>
        <button onClick={this.props.navigeerBetaal} className="GerechtenWinkelmandje_button">Betaal</button>
      </section>
    );
  }
}

export default GerechtenWinkelmandje
