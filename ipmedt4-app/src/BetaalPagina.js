import React from 'react';
import './BetaalPagina.css';
import {Link, withRouter} from 'react-router-dom';

import TestHeader from "./TestHeader";
import BezorgLocatie from './BezorgLocatie';
import BetaalMethode from './BetaalMethode';

import axios from "axios";



class BetaalPagina extends React.Component {

  knop = () => {
      document.getElementById("js--errortekst").style.display = "block";
      document.getElementById("js--placeholder").style.display = "none";
  }

  render() {
    return(
        <section>
          <TestHeader />
          <section className="betaalpagina__section">

            <h2 className="betaal__header">Bezorg locatie</h2>
            <BezorgLocatie />
            <h2 className="betaal__header">Betaal methode</h2>
            <BetaalMethode/>

            <h2 className="betaal__header">Uw bestelling</h2>
            <p>Heeft u een voucher?</p>
            <input className="betaalpagina__section__input"/>
            <button className="betaalpagina__section__button voucherknop" type="submit" onClick={this.knop} >Check de voucher</button><br />
            <p id="js--errortekst" className="betaalpagina__section__errorbericht">Dit is helaas geen geldige voucher</p>
            <p id="js--placeholder" className="betaalpagina__section__placeholder">.</p>
            <button className="betaalpagina__section__button betaalknop" type="submit"><Link to="/bezorgpagina/1" className="link">Bestelling afronden</Link></button>
          </section>
        </section>
    );
  }
}

export default BetaalPagina;
