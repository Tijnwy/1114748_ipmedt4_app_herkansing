import React from 'react';
import {Link, withRouter} from 'react-router-dom';

class Navbar extends React.Component{
    LogOut(e){
        e.preventDefault();
        localStorage.removeItem('usertoken');
        this.props.push('/');
    }

    render(){
        const loginRegLink = (
            <ul>
                <li>
                    <Link to="/login">
                        Login
                    </Link>
                </li>
                <li>
                    <Link to="/register">
                        Register
                    </Link>
                </li>
            </ul>
        );

        const userLink = (
            <ul>
                <li>
                    <Link to="/profile">
                        profile
                    </Link>
                </li>
                <li>
                    <a href="/" onClick={this.LogOut.bind(this)}>
                        logout
                    </a>
                </li>
            </ul>
        );
        return(
            <nav>
                <button type="button" data-toggle="collapse" data-target="#navbar1" aria-controls="navbar1" aria-expanded="false" aria-label="Toggle navigation">
                    <span></span>
                </button>
                <div id="navbar1">
    
                </div>
                <ul>
                    <li>
                        <Link to="/"> Home</Link>
                    </li>
                </ul>
                {localStorage.usertoken ? userLink : loginRegLink}
            </nav>
        );
    }
}

export default withRouter(Navbar)