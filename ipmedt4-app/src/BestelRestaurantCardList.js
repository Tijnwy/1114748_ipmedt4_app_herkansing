import React from 'react';
import "./BestelRestaurantCardList.css";
import BestelRestaurantCard from "./BestelRestaurantCard";
// import logo_resaurant from "./images/logo_restaurant.png";
import axios from 'axios';

// To do: API call maken zorgen dat er een map komt met een array erin die objecten van
// bestelrestaurantcard maakt en de json moet een naam terug geven een logo terug geven een waardering terug geven
// Nieuw in API: beschrijving erin nieuw maken zodat dit ook kan worden terug gegeven
// Categorie moet een if statement worden geschreven waarin de json wordt gechecked wat voor categorie die heeft en dan de daarbij horende front end foto
// Er moet hier een state komen van categorie, wanneer er op categorie gefilterd wordt, mogen alleen maar gerechten met de juiste categorie weergeven worden


class BestelRestaurantCardList extends React.Component{

  restaurantGekozen = (restaurant) =>{
    this.props.restaurantGekozen(restaurant)
  }

  /* hardcoded voorbeeld als geen database connectie
  <BestelRestaurantCard naam="happy italiano" imgCategorieSrc={pizza} imgLogoSrc={logo_restaurant} waardering="5/5"
  beschrijving="Hallo dit is een mooie beschrijving van minder dan 75 karakters"/>
  <BestelRestaurantCard naam="happy italiano" imgCategorieSrc={pizza} imgLogoSrc={logo_restaurant} waardering="5/5"
  beschrijving="Hallo dit is een mooie beschrijving van minder dan 75 karakters"/>
  <BestelRestaurantCard naam="happy italiano" imgCategorieSrc={pizza} imgLogoSrc={logo_restaurant} waardering="5/5"
  beschrijving="Hallo dit is een mooie beschrijving van minder dan 75 karakters"/>
  <BestelRestaurantCard naam="happy italiano" imgCategorieSrc={pizza} imgLogoSrc={logo_restaurant} waardering="5/5"
  beschrijving="Hallo dit is een mooie beschrijving van minder dan 75 karakters"/> */

  render(){
    const BASE_URL_CATEGORIE_IMG = process.env.REACT_APP_API_URL + 'images/categorie_image/'
    return(
      <section className="Bestel_restaurant_list">
        {this.props.filteredRestaurantList.map((restaurant) =>{
          return(
            <BestelRestaurantCard restaurantCardClicked={this.restaurantGekozen} key={restaurant.naam} naam={restaurant.naam} imgLogoSrc={restaurant.logo}
            waardering={restaurant.rating} beschrijving={restaurant.beschrijving}  categorie={restaurant.categorie}
            imgCategorieSrc={BASE_URL_CATEGORIE_IMG + restaurant.categorie + ".jpg"}/>
          )
        })}
      </section>
    );
  }
}

export default BestelRestaurantCardList;
