import React from 'react';
// import {BrowserRouter as Router, Route} from "react-router-dom";
import BestelHeader from "./BestelHeader";
import SnelMenu from "./SnelMenu";
import BestelRestaurantCardList from "./BestelRestaurantCardList";
import "./KlantBestellen.css";
// import "./Login.css";
import axios from 'axios';
import {keuzeRestaurantActie, restaurantListActie, adresActie, keuzeRestaurantCategorieActie} from './actions'
import { connect } from 'react-redux';
import GoogleMapReact from 'google-map-react';
import cloneDeep from 'lodash/cloneDeep';


const google = window.google;

class KlantBestellen extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      restaurantList: [],
      filteredRestaurantList: [],
      categorieFilteredRestaurant: []
    }
    this.klantPostcode = "";
    this.klantPostcodeLat = "";
    this.klantPostcodeLng = "";
    this.restaurantsPostcodesLat = [];
    this.restaurantsPostcodesLng = [];
    this.restaurantNaam = [];
    this.counter = 0;
    this.updatecounter = 0;
    this.afstand = [];
    this.repeat = false;
    this.geocoder = new google.maps.Geocoder();
    this.categorie = "";
    this.restaurantCoordinatenOpgevraagd = false;
  }

  //Zet de state van restaurantlist.
  //Door de setstate wordt componentDidUpdate aangeroepen
  //Sla hetzelfde op in de redux store, zodat het later in de code kan worden gebruikt als copy.
  //Dit als reden, omdat een deep copy zorgde voor inconsistentheid elke 4-5 keer als het werd aangeroepen.
  componentDidMount(){
    axios.get(process.env.REACT_APP_API_URL + 'api/restaurant/klantbestelling').then(res =>{
        console.log(res);
        this.setState({restaurantList: res.data})
        this.props.restaurantListActie(res.data)
      });
  }

  //Wanneer de gebruiker van homepage komt, word adressSubmitted via componentdidupdate aangesproken.
  //Dit mag maar een keer gebeuren, omdat de updates hierna vanuit de bestelheader worden gedaan.
  //Met uizondering wanneer er op een categorie gefiltered wordt zie snelMenuItemClicked
  componentDidUpdate(pP, pS){
    this.updatecounter++;
    console.log(this.updatecounter)
    console.log('haaai')
    if(this.updatecounter <= 1){
      if(this.klantPostcode !== "geen postcode" && this.klantPostcode !== ""){
        this.adressSubmitted(this.klantPostcode)
      }
      else if(this.props.adresKlantBestelling !== ""){
        console.log("HIJ KOMT HIER")
        this.adressSubmitted(this.props.adresKlantBestelling)
      }
    }
  }

  //Wanneer er op een categorie wordt geklikt komt er een nieuwe lijst met gefilterde woonplaats + gefilterde categorie.
 snelMenuItemClicked = categorie => {
      this.categorie = categorie;
      let restaurantListCopy = JSON.parse(JSON.stringify(this.state.filteredRestaurantList));
      let categorieFilter = []
      if(categorie !== "Zie Alle" && categorie !== ""){
        for(let i = 0; i < this.state.filteredRestaurantList.length; i++){
          if(restaurantListCopy[i].categorie === categorie){
            categorieFilter.push(restaurantListCopy[i])
          }
        }
        console.log(categorieFilter)
        this.setState({categorieFilteredRestaurant: categorieFilter})
      }
      else{
        this.setState({categorieFilteredRestaurant: []})
      }
  }

  //Wanneer er op een restaurant wordt geklikt, redirect hem naar de volgende page en sla de restaurantsnaam op in redux store
  bestelRestaurantCardClicked = title => {
    console.log("Het gekozen restaurant is " + title);
  }

  //Functie om te zien welke postcode de gebruiker heeft ingevoerd
  //Checked of die een postcode heeft mee gekregen vanuit het child component bestelheader
  //Anders checked of de redux adres reducer niet iets leegs mee geeft, als dat zo is, gebruik postcode van redux reducer (Wanneer de gebruiker van de homepage af komt)
  //Anders geeft die geen postcode mee, omdat er nog geen postcode is ingevuld waar dan ook
  adressSubmitted = (postcode) =>{
    this.categorie = "";
    if(typeof postcode !== "undefined"){
      this.klantPostcode = postcode.split(' ').join('').slice(0,6);
    }

    else{
      this.klantPostcode = "Geen postcode"
    }
    //klat lat en lng van klatn postcode
    console.log(this.klantPostcode)
    this.latLng(this.klantPostcode, true, "geen")
    //Krijg ook alle postcode Lng en Lat van de restaurants dit doet die maar 1 keer in zijn cycle zodat de google API
    //Niet teveel requests krijgt
    if(this.restaurantCoordinatenOpgevraagd == false){
      let restaurantsPostcodes = [];
      let restaurantsNamen = [];
      this.state.restaurantList.map(function(restaurant){
        restaurantsPostcodes.push(restaurant.postcode)
        restaurantsNamen.push(restaurant.naam)
      })
      if(this.klantPostcode !== ""){
        for(let i = 0; i < restaurantsPostcodes.length; i++){
          this.latLng(restaurantsPostcodes[i], false, restaurantsPostcodes.length, restaurantsNamen[i])
        }
      }
    }
  }

  //vult lat lng in van de klantPostcode's en restaurantpostcodes
  //De lijsten kunnen in een andere volgorde staan dan hoe het staat in restaurantList state
  //Hierom wordt er bij restaurants ook een lijst met restaurant namen gevuld, zodat duidelijk is welke lat en lng bij welke restaurant naam horen aangezien die listen gelijk lopen.
  latLng = (postcode, user, postcodeListLength, restaurantNaam) =>{
    let apiKey = 'AIzaSyAaXREgifgVpZsMR_gCN7UPnqfliuBI4wc';
    let lat = "";
    let lng = "";
    setTimeout(function(){
      this.geocoder.geocode( { 'address': postcode}, function googleStuf(results, status) {
       if (status == 'OK') {
         if(user){
           lat = results[0].geometry.location.lat()
           lng = results[0].geometry.location.lng()
           this.klantPostcodeLat = lat
           this.klantPostcodeLng = lng
           if(this.counter == this.state.restaurantList.length){
             this.calcDistance()
           }
         }
         else{
           lat = results[0].geometry.location.lat()
           lng = results[0].geometry.location.lng()
           this.restaurantsPostcodesLat.push(lat);
           this.restaurantsPostcodesLng.push(lng);
           this.restaurantNaam.push(restaurantNaam);
           this.counter++;
           //Wannee restaurantCoordinatenOpgevraagd op true staat, worden zie niet meer opnieuw gevraagd wanneer er een nieuw adres wordt ingevoerd
           //Dit is om te verkomen dat de Geocode api teveel requests krijgt en errors geeft.
           if(this.counter == this.state.restaurantList.length){
             this.restaurantCoordinatenOpgevraagd = true
             this.calcDistance()
           }
         }
       } else {
         alert('Geen correcte postcode ingevuld!');
       }
     }.bind(this));
   }.bind(this), 10);
}

  //Berekening van de afstanden tussen de klant en de verschillende restaurants
  //De afstand is tussen 2 verschillende coordinaten, dus de werkelijke afstand is meer, hiermee is rekening gehouden in de methode filterRestaurantsOpAfstand()
  calcDistance = () =>{
    let klantLocatie = new google.maps.LatLng(this.klantPostcodeLat, this.klantPostcodeLng);
    let restaurantlocatie = []
    for(let i = 0; i < this.state.restaurantList.length; i++){
       restaurantlocatie.push(new google.maps.LatLng(this.restaurantsPostcodesLat[i], this.restaurantsPostcodesLng[i]));
       this.afstand.push((google.maps.geometry.spherical.computeDistanceBetween(klantLocatie, restaurantlocatie[i]) / 1000))
    }
    this.filterRestaurantsOpAfstand()
    }

  //filtered boven de 5 kilometer afstand
  filterRestaurantsOpAfstand = () =>{
    let index = 0;
    let restaurants = JSON.parse(JSON.stringify(this.props.restaurantListRedux));
    for(let i = 0; i < this.state.restaurantList.length; i++){
      console.log(this.afstand[i])
      if(this.afstand[i] >= 5){
        index = restaurants.findIndex(x => x.naam == this.restaurantNaam[i]);
        restaurants.splice(index, 1)
      }
    }
    let tussenbrugrestaurants = Object.assign(restaurants)
    this.setState({filteredRestaurantList: tussenbrugrestaurants})
    this.clean()
  }

  clean = () =>{
    this.klantPostcodeLat = "";
    this.klantPostcodeLng = "";
    this.afstand = [];
  }

  //restaurant gekozen en er wordt genavigieerd naar de Gerechten pagina
  restaurantGekozen = (restaurant) =>{
    this.props.keuzeRestaurantActie(restaurant);
    this.props.history.push('/GerechtenKlant');
  }

  openCloseSnelmenu = (button, snelMenu, isOpen) =>{
    let sectionKlantbestellen = document.getElementById('klantBestellen--id')
    sectionKlantbestellen.classList.toggle('SnelMenu_modal')
    if(isOpen){
      snelMenu.classList.toggle('Snelmenu')
      snelMenu.classList.toggle('Snelmenu_display')
    }
    else{
      snelMenu.classList.toggle('Snelmenu')
      snelMenu.classList.toggle('Snelmenu_display')
    }
  }

  render(){
  console.log(this.state.categorieFilteredRestaurant);
  console.log(this.state.filteredRestaurantList);
  let restauranten = []
  if(this.categorie == "Zie Alle" || this.categorie == ""){
    restauranten = this.state.filteredRestaurantList
  }
  else{
    restauranten = this.state.categorieFilteredRestaurant
  }

  return(
    <section id="klantBestellen--id">
      <BestelHeader adressSubmitted={this.adressSubmitted}/>
      <SnelMenu openCloseSnelmenuModal={this.openCloseSnelmenu} className="SnelMenuParent" snelMenuItemClicked={this.snelMenuItemClicked}/>
      <BestelRestaurantCardList restaurantGekozen={this.restaurantGekozen} filteredRestaurantList={restauranten}
      bestelRestaurantCardClicked={this.bestelRestaurantCardClicked}/>
    </section>
  );
  }
}

const mapStateToProps = (state) => ({
	adresKlantBestelling: state.adresKlantBestelling,
  restaurantListRedux: state.restaurantListRedux,
  gekozenRestaurant: state.gekozenRestaurant,
  gekozenCategorie: state.gekozenCategorie
});

const mapDispatchToProps = {
  adresActie,
  restaurantListActie,
  keuzeRestaurantActie,
  keuzeRestaurantCategorieActie
}

export default connect(mapStateToProps, mapDispatchToProps ) (KlantBestellen);
