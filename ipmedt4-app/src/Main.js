import React from 'react';
import LandingspageHeader from './LandingspageHeader.js';
import Info from './info.js';
import Landingspagepicure from './Landingspagepicture';

import {BrowserRouter as Router, Route} from "react-router-dom";
import Navbar from './Navbartest';
import Login from './Login';
import Register from './Register';
import Profile from './Profile';

function Main() {
    return (
        <section>
        <LandingspageHeader/>
        <Landingspagepicure/>
        <Info/> 
        </section>
    );

}


export default Main;