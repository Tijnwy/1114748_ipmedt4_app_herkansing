import React, {useState} from 'react';
import './App.css';

import LandingspageHeader from './LandingspageHeader.js';
import Info from './info.js';
import Landingspagepicure from './Landingspagepicture';

import {BrowserRouter as Router, Route} from "react-router-dom";
// import Navbar from './Navbartest';
import Login from './Login';
import Register from './Register';
import Profile from './Profile';
import Main from "./Main";
import Delivery from "./Delivery";
import RegisterBezorger from './RegisterBezorger';
import RegisterRestaurant from './RegisterRestaurant';
import RegisterRestaurant2 from "./RegisterRestaurant2";
import EditUser from './EditUser';
import BezorgerPagina from './BezorgerPagina';
import KlantBestellen from './KlantBestellen';
import GerechtInvoer from './restaurant/GerechtInvoer.js';
import RestaurantDashboard from "./restaurant/RestaurantDashboard.js";
import GerechtOverzicht from "./restaurant/GerechtOverzicht.js";
import GerechtBewerken from "./restaurant/GerechtBewerken.js";
import RestaurantBestellingen from "./restaurant/RestaurantBestellingen.js";
import SideBar from './SideBar';
import WriteReviewModal from './WriteReviewModal';
import BetaalPagina from './BetaalPagina';
import FavorietenList from "./FavorietenList";

class App extends React.Component {

  constructor(){
    super();
  }

  // componentDidMount(){
  //   console.log("mount app");
  //   localStorage.removeItem('usertoken');

  // }

  render(){
  return (


    <Router>
    <div>
        <Route exact path="/" component={Main}/>
        <Route exact path="/profile" component={Profile}/>
        <Route exact path="/registerklant" component={Register}/>
        <Route exact path="/login" component={Login}/>
        <Route exact path="/profile" component={Profile}/>
        <Route exact path="/registerbezorger" component={RegisterBezorger}/>
        <Route exact path="/registerrestaurant" component={RegisterRestaurant}/>
        <Route exact path="/favorieten" component={FavorietenList} />
        <Route exact path="/betalen" component={BetaalPagina} />
        <Route exact path="/gerechtinvoer" component={GerechtInvoer}/>
        <Route exact path="/registerrestaurant2" component={RegisterRestaurant2}/>
        <Route exact path='/edituser' component={EditUser}/>
        <Route exact path="/bezorgerpagina" component={BezorgerPagina}/>
        <Route exact path="/bezorgpagina" component={Delivery}/>
        <Route exact path="/restaurantdashboard" component={RestaurantDashboard}/>
        <Route exact path="/gerechtoverzicht" component={GerechtOverzicht}/>
        <Route exact path="/gerechtbewerken/:id" render={props => <GerechtBewerken{...props}/> }/>
        <Route exact path="/klantbestellen" component ={KlantBestellen} />
        <Route exact path="/restaurantbestellingen" component ={RestaurantBestellingen}/>
    </div>
    </Router>


  );
}
}

export default App;
