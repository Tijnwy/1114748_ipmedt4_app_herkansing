import React from 'react';
import './GerechtenKlantKeuze.css'
import star from "./images/star.png";

class GerechtenKlantKeuze extends React.Component{
  componentDidMount(){

  }

  gerechtClicked = () =>{
    this.props.gerechtClicked(this.props.titel, this.props.prijs, this.props.id);
  }

  render(){
    const BASE_URL_CATEGORIE_IMG = process.env.REACT_APP_API_URL + 'images/categorie_image/'
    return(
      <section className="GerechtenKlantKeuze" onClick={this.gerechtClicked}>
        <h1 className="GerechtenKlantKeuze_titel">{this.props.titel}</h1>
        <p className="GerechtenKlantKeuze_beschrijving">{this.props.beschrijving}</p>
        <p className="GerechtenKlantKeuze_prijs">{this.props.prijs + "€"}</p>
        <img className="GerechtenKlantKeuze_foto" src={BASE_URL_CATEGORIE_IMG + this.props.imgSrc + ".jpg"} />
      </section>
    );
  }
}

export default GerechtenKlantKeuze
