import React from 'react';
import "./SnelMenuItem.css";


class SnelMenuItem extends React.Component{
  onSnelMenuItemClick = () =>{
    this.props.snelMenuItemClicked(this.props.title)
  }
  render(){
    return(
      <h2 className="Snelmenuitem" onClick={this.onSnelMenuItemClick}>{this.props.title}</h2>
    );
  }
}

export default SnelMenuItem
