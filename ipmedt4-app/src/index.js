import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import Delivery from './Delivery';
import { Provider } from 'react-redux';
import {createStore} from 'redux';
import allReducers from './reducers';

export const store = createStore(allReducers,
window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);


ReactDOM.render(
  //Redux
  //Provider onderdeel van redux
  <Provider store={store}>
  <React.StrictMode>
    <App />
  </React.StrictMode>
  </Provider>,
  document.getElementById('root')
);
