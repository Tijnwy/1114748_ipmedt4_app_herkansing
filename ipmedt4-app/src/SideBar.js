import React from 'react';
import "./SideBar.css";
import {getProfile} from "./UserFunctions"
import {Link, withRouter} from 'react-router-dom';

class SideBar extends React.Component{

    constructor(props){
        super(props);

        this.state={
            naam: '',
            nav1: '',
            nav2: '',
            nav3: '',
            nav4: '',
            nav5: '',
            nav6: '',
            href1: '',
            href2: '',
            href3: '',
            href4: '',
            href5: '',
            herf6: ''
        }

        this.logOut= this.logOut.bind(this);

    }

      
      closeNav() {
        document.getElementById("js--nav").style.width = "0";
      }

      componentDidMount() {
          if(localStorage.usertoken == null){
              console.log("get profile wordt niet uitgevoerd");
          }
          else{
              console.log("get profile wordt wel uitgevoerd");
              
            getProfile().then(res => {
                if(res){
                console.log(res);
                    
                    if(res.user.rol == 'klant'){
                        this.setState({
                            nav1: 'Kortingscodes',
                            nav2: 'Gegevens',
                            nav3: 'Bestellen',
                            nav4: 'Favoriete restaurants',
                            href1: '',
                            href2: 'edituser',
                            href3: 'klantbestellen',
                            href4: 'favorieten'
    
                        })
                    }
    
                    if(res.user.rol == 'restaurant'){
                        this.setState({
                            nav1: 'Dashboard',
                            nav2: 'Gegevens',
                            href1: 'restaurantdashboard',
                            href2: 'edituser'
                        })
                    }
    
                    if(res.user.rol == 'bezorger'){
                        this.setState({
                            nav1: 'Gegevens',
                            nav2: 'Bezorgerdashboard',
                            href1: 'edituser',
                            href2: 'bezorgerpagina'
                        })
                    }
    
                
                this.setState({
                    naam: res.user.naam
                })
            }
            else{
                this.setState({
                    naam: 'navigatie'
                }) 
            }
            })
        
    }
}

    logOut(e){
        e.preventDefault();
        localStorage.removeItem('usertoken');
        // this.props.history.push(`/`);
        window.location.reload();

        
    }



    
    render(){
        return(
            <nav id="js--nav" style={{ width: this.props.width}} className="navigation">
                <ul className="navigation_list">
                    <li className="navigation_list_item navigation_list_naam">{this.state.naam}</li>
                    <li className="navigation_list_item"><Link className="navigation_list_item_link" to="/">Home</Link></li>
                    <li className="navigation_list_item"><Link className="navigation_list_item_link" to={this.state.href1}>{this.state.nav1}</Link></li>
                    <li className="navigation_list_item"><Link className="navigation_list_item_link" to={this.state.href2}>{this.state.nav2}</Link></li>
                    <li className="navigation_list_item"><Link className="navigation_list_item_link" to={this.state.href3}>{this.state.nav3}</Link></li>
                    <li className="navigation_list_item"><Link className="navigation_list_item_link" to={this.state.href4}>{this.state.nav4}</Link></li>
                    <li className="navigation_list_item"><Link className="navigation_list_item_link" to={this.state.href5}>{this.state.nav5}</Link></li>
                    <li className="navigation_list_item"><Link className="navigation_list_item_link" to={this.state.href6}>{this.state.nav6}</Link></li>
                    <li onClick={this.logOut} className="navigation_list_item navigation_list_logout"><Link className="navigation_list_item_link" to="/">Log Uit</Link></li>
                </ul>
            </nav>
        );
    }
}

export default SideBar;