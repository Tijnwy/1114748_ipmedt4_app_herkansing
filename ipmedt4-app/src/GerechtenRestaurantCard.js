import React from 'react';
import './GerechtenRestaurantCard.css'
import star from "./images/star.png";
import {registerFavoriet} from "./UserFunctions";
import {getProfile} from "./UserFunctions"
class GerechtenRestaurantCard extends React.Component{

  //Kenny je kan eventueel this.props.naam gebruiken, dit is de naam van het restaurant.
  reviewClick = () =>{
    this.props.showReviewModal(this.props.naam);
  }

  constructor() {
    super()
    this.state = { email: '' }
  }

  componentDidMount() {

    getProfile().then(response => {
      this.setState({email: response.user.email})
    })
  }

  favorietClick = () =>{

    const newFavoriet = {
      naam: this.props.naam,
      email: this.state.email
      })
    }
    registerFavoriet(newFavoriet);
    window.location.href="/favorieten"
  }

  render(){
    return(
      <section className="GerechtenRestaurantCard">
        <img className="GerechtenRestaurantCard_logo" src={this.props.logo} atl="Logo restaurant" />
        <h2 className="GerechtenRestaurantCard_naam">{this.props.naam}</h2>
        <h3 className="GerechtenRestaurantCard_beoordeling">{"Rating: " + this.props.rating + "/5"}
        <img className="GerechtenRestaurantCard_beoordeling_ster" src={star} alt="Ster"/>
        </h3>
        <p className="GerechtenRestaurantCard_beschrijving">{this.props.beschrijving}</p>
        <div className="GerechtenRestaurantCard_buttons">
          <button className="GerechtenRestaurantCard_buttons--review" onClick={this.reviewClick} type="button">Reviews</button>
          <button className="GerechtenRestaurantCard_buttons--favoriet" onClick={this.favorietClick} type="button">Favoriet</button>
        </div>
      </section>
    );
  }
}

export default GerechtenRestaurantCard
