import React from 'react';
import "./Delivery.css";
import DeliveryPageHeader from './DeliveryPageHeader.js';
import DeliveryTimeTracker from './DeliveryTimeTracker.js';
import DeliveryLocationTracker from './DeliveryLocationTracker.js'


class Bezorgtijd extends React.Component {

  render(){
    return(
      <section>
        <DeliveryPageHeader
        titel="Bedankt voor ondersteuning van de lokale ondernemer!"
        ondertitel="Volg hier uw bestelling." />
        <DeliveryTimeTracker ordernummer={this.props.match.params.ordernummer}/>
        <DeliveryLocationTracker ordernummer={this.props.match.params.ordernummer}/>
      </section>
    );
  };
}





export default Bezorgtijd;
