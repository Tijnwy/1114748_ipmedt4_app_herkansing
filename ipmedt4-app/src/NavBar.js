import React from 'react';
import "./NavBar.css";


const Navbar = () => {
    return (
        <section className="navbar">
            <nav className="navbar_navigation">
                <ul className="navbar_navigation_list">
                    <li className="navbar_navigation_list_item">Klantaccount</li>
                    <li className="navbar_navigation_list_item">Dashboard</li>
                    <li className="navbar_navigation_list_item">Bestellingen</li>
                    <li className="navbar_navigation_list_item">Reviews</li> 
                    <li className="navbar_navigation_list_item">Restaurantpagina</li>  
                    <li className="navbar_navigation_list_item">Profiel</li>              
                </ul>
            </nav>
        </section>
    )
}

export default Navbar;