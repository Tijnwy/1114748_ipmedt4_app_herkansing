import React from 'react'
import "./BezorgerPagina.css"
import "./LandingspageHeader.css"
import BezorgerAlleBestellingen from './BezorgerAlleBestellingen';
import BezorgerGeclaimdeBestellingen from './BezorgerGeclaimdeBestellingen';
import BezorgerBezorgdeBestellingen from './BezorgerBezorgdeBestellingen';
import BezorgerGegevens from './BezorgerGegevens';
import DeliveryPageHeader from './DeliveryPageHeader';
import axios from 'axios';
import {getProfile} from "./UserFunctions";
import BezorgerLocatieMap from './BezorgerLocatieMap';

class BezorgerPagina extends React.Component{
  constructor() {
    super()
    this.child1Ref = React.createRef();
    this.child2Ref = React.createRef();
    this.child3Ref = React.createRef();
    this.state = {
      restaurant: "",
      status: "",
    }
  }

  closeModal = () => {
    document.getElementById("myModal").style.display = "none";
  }

  openModal = () => {
    document.getElementById("myModal").style.display = "block";
  }

  updateStatus = (ordernummer, status, restaurantNaam) => {
    if (status == "geclaimd" && status == this.state.status && restaurantNaam != this.state.restaurant) {
      console.log(this.state.restaurant);
      this.openModal();
    }
    else if (status == "bezorgd") {
      console.log(this.state.restaurant);
      getProfile().then(res => {
        console.log(res);
        let email = res.user.email;

        return axios
        .patch(process.env.REACT_APP_API_URL + 'api/bestelling/updateStatus/'+ ordernummer + "/" + status + "/" + email, {
            headers:{'Content-Type': 'application/json'},
            _method: 'patch'

        })
        .then(res => {
            console.log(res);

        })
        .catch(err => {
            console.log(err);
        })
      })
      this.setState({status: "bezorgd"})

      setTimeout(function() {
        this.child1Ref.current.updateStateBestellingen();
        this.child2Ref.current.updateStateBestellingen();
        this.child3Ref.current.updateStateBestellingen();
      }.bind(this), 200);
    }
    else {
      console.log(this.state.restaurant);
      getProfile().then(res => {
        console.log(res);
        let email = res.user.email;

        return axios
        .patch(process.env.REACT_APP_API_URL + 'api/bestelling/updateStatus/'+ ordernummer + "/" + status + "/" + email, {
            headers:{'Content-Type': 'application/json'},
            _method: 'patch'

        })
        .then(res => {
            console.log(res);

        })
        .catch(err => {
            console.log(err);
        })
      })
      this.setState({status: "geclaimd"})
      this.setState({restaurant: restaurantNaam})

      setTimeout(function() {
        this.child1Ref.current.updateStateBestellingen();
        this.child2Ref.current.updateStateBestellingen();
      }.bind(this), 200);
    }
  }

  render(){
    return (
    <section>
      <DeliveryPageHeader
      titel="Welkom op de bezorgerpagina!"
      ondertitel="De beheeromgeving voor jou als bezorger."/>
      <article className="bezorgerPagina">
          <section className="bezorgerPagina_grid1">
              <BezorgerGegevens/>
              <BezorgerLocatieMap/>
          </section>
          <section className="bezorgerPagina_grid2">
              <BezorgerAlleBestellingen ref={this.child1Ref} updateStatus={this.updateStatus}/>
              <BezorgerGeclaimdeBestellingen ref={this.child2Ref} updateStatus={this.updateStatus} />
              <BezorgerBezorgdeBestellingen ref={this.child3Ref} />
          </section>
      </article>
      <section onClick={this.closeModal} id="myModal" className="modal">
        <section className="modal_content">
          <span onClick={this.closeModal} className="close">&times;</span>
          <p><b>Deze actie is niet mogelijk!</b></p><br />
          <p>Je kunt alleen meerdere bestellingen tegelijk van hetzelfde restaurant claimen.</p>
          <button onClick={this.closeModal} className="modal_content_button">Ik heb het begrepen!</button>
        </section>
      </section>
    </section>
    );
  }
}

export default BezorgerPagina;
