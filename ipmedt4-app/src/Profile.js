import React from 'react';
import { getProfile } from './UserFunctions';
import axios from 'axios';
import jwt_decode from 'jwt-decode';


class Profile extends React.Component
{
    constructor() {
        super()
        this.state = {
            naam: '',
            email: ''
        }
    }

    componentDidMount() {
        
        getProfile().then(res => {
            console.log(res);
            console.log("test" + res.user.email)
            
            this.setState({
                naam: res.user.naam,
                email: res.user.email
            })
        })
    }

   


    render(){
        return(
            <div>
                <h1>Profile</h1>
                <p>{this.state.naam}</p>
                <p>{this.state.email}</p>
            </div>
        );
    }
}

export default Profile;

